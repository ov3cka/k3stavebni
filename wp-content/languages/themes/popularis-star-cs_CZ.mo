��          \      �       �      �      �      �   L  �   	   I  '   S     {  *  �  
   �     �     �  a  �  	   Q  '   [     �                                       About The Author Homepage main menu Popularis Star Popularis Star is a fast, clean and modern-looking responsive multipurpose theme for WordPress. Theme works perfect with any of favorite page builders like Elementor, Beaver Builder, SiteOrigin, Thrive Architect, Divi, Visual Composer, etc. Popularis Star is WooCommerce ready, lightweight, easy to use, responsive and SEO friendly. Themes4WP https://populariswp.com/popularis-star/ https://themes4wp.com/ PO-Revision-Date: 2020-03-06 10:06:15+0000
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;
X-Generator: GlotPress/2.4.0-alpha
Language: cs_CZ
Project-Id-Version: Themes - Popularis Star
 O autorovi Menu domovské stránky Popularis Star Popularis Star je rychlá, jednoduchá a moderně vypadající víceúčelová WordPress šablona. Funguje perfektně s jakýmkoli oblíbeným tvůrcem stránek, jako je Elementor, Beaver Builder, SiteOrigin, Thrive Architect, Divi, Visual Composer atd. Šablona je připavena pro WooCommerce, snadno použitelná, responsivní a optimalizvaná pro SEO. Themes4WP https://populariswp.com/popularis-star/ https://themes4wp.com/ 