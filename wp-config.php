<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'k3stavebni' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '+kSc)|DkVd@6Jt --b=p3r6!7t8djJo-kS|eZ(6><e;V{1mhM%-r}<h|LgwCx$U{');
define('SECURE_AUTH_KEY',  '#&SVwP.B-2dswOiTabNHdXofFqHX~,0J|2Nu6)$ (XW9OX|7dl*(O:p[-f{3{byD');
define('LOGGED_IN_KEY',    'l50Vbz8.Y$l[ZQe2B~!~:~zM|~U7Vli2.P.>[qH%L@jWw`I6%Uij#h -(q~+8!W-');
define('NONCE_KEY',        '5^[OR+l:.Ij7!N?<!|0b3qG$%6V -,?&M~@ f][KPn|}F}]Ak5[tZt@[!n|^x{u>');
define('AUTH_SALT',        'E>EE|~>fZjgSP[rqhM|Xkq-#x7{?[wux gSh~{nA874-zkBt8`hdXeSam}|Pr|1{');
define('SECURE_AUTH_SALT', 'ttG0lPa,}J/L[%0+Et)N8mW~cf_:pK-RY5r;RRW&i%istU9|CAoU ia9r|]}jF>1');
define('LOGGED_IN_SALT',   'LoJfYkYg!gy_!<xiJ O(=V*Zkd<Q~Ka?_p)Zy.YGj;VLY&vMcv-5IT+t%Je,2+#C');
define('NONCE_SALT',       't/-M(2@jydltM^U.Se#tDhkoh[|k%4bEMN54%4$s1Y,gDQ|ACuVCfO#B`uD3s/n1');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
